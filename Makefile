# Makefile

# alias dcd="docker-compose -f $PWD/docker-compose-dev.yml"
# alias dcp="docker-compose -f $PWD/docker-compose-prod.yml"

WAIT_FOR = 5

STAGE = dev
DCF = docker-compose -f ${PWD}/docker-compose-${STAGE}.yml

SVC = web
BUILD = # --build

# make up BUILD=--build STAGE=prod
up:
	${DCF} up -d ${BUILD}
	sleep ${WAIT_FOR}; ${DCF} logs ${SVC}

# remove volumes!
down:
	${DCF} down --volumes --remove-orphans

stop:
	${DCF} stop

ps: status
status:
	${DCF} ps

# make logs LOGSF=-f
logs:
	${DCF} logs ${LOGSF} ${SVC}

shell:
	${DCF} exec ${SVC} /bin/sh

create_admin:
	${DCF} exec web python manage.py createsuperuser

psql:
	${DCF} exec db psql --username=hello_django --dbname=postgres

img_clean:
	docker image prune
