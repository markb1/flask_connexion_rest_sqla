## Python REST APIs With Flask, Connexion, and SQLAlchemy

Ref: *RealPython* Tutorial, https://realpython.com/flask-connexion-rest-api/


### Project Setup

#### Create the virtual-env

``` shell

  mkdir flask-connexion-rest-api

  mkvirtualenv -p $(which python3) -a $PWD/flask-connexion-rest-api \
    flask-connexion-rest-api
```


#### Create a basic Flask "api" project

* Edit, a new ``server.py`` basic ``/`` api service.

* Edit, a new ``templates/home.html`` template


#### Run the basic API service

Browse to: http://127.0.0.1/

``` shell
  python server.py
```


### Add Connexxion for REST-api endpoint(s)

#### Create a Dockerfile & docker-compose.yml

``` shell
  
  vim Dockerfile

  vim docker-compose.yml
  
```

#### Create or modify "Flask settings" for this app

* Create a ``.env.dev`` in the project-root.

* Update ``hello_django/settings.py`` to pick-up those settings


### Add Py client & Postgresql server

Edit: ``Dockerfile``; ``docker-compose.yml``;
``hello_django/settings.py`` (PgSQL DB config); ``.env.dev`` and ``requirements.txt``

#### Build the new image, start the two containers

``` shell

  # start 'web' and 'db' containers
  docker-compose up -d --build

  # then, run the DB migrations!


```

#### Check that default DB tables are created

``` shell

  docker-compose exec db psql --username=api_luser --dbname=flask_api_dev

  # in 'db' container -- list DBs

  flask_api_dev=#  \l

  flask_api_dev=#  \c flask_api_dev
  -- You are now connected to database "flask_api_dev" as user "api_luser".

  # describe 'flask_api_dev` table

  flask_api_dev=#  \dt

```

To check for created Docker *volume*:

``` shell

   docker volume list | egrep -i 'postgres_data|volume name'  # get the volume name
   
   docker volume inspect  django_on_docker_tio_postgres_data

```

