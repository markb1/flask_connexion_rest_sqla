from pprint import pformat

from flask import (
    Flask,
    render_template,
)

import connexion
from flask_debugtoolbar import DebugToolbarExtension
from flask import current_app

from api import people


# Create the application instance
event_api = connexion.App(__name__, specification_dir='./')

event_api.add_api('swagger.yml')

evapp = event_api.app

# set a 'SECRET_KEY' to enable the Flask session cookies
evapp.config['SECRET_KEY'] = 'Xyzzy@12345x3219847090#$UO'
evapp.config['TEMPLATE_FOLDER'] = 'templates'
evapp.config['ENV'] = 'development'

evapp.config['DEBUG_TB_ENABLED'] = True
evapp.config['DEBUG_TB_HOSTS'] = ('127.0.0.1', 'localhost.localdomain',)

toolbar = DebugToolbarExtension(evapp)

evapp.debug = True
evapp.logger.info(pformat(evapp.config))


# Create a URL route in our application for "/"
@event_api.route('/')
def home():
    """
    This function just responds to the browser ULR
    localhost:5000/

    :return:        the rendered template 'home.html'
    """
    return render_template('home.html')


# If we're running in stand alone mode, run the application
if __name__ == '__main__':
    event_api.run(debug=True)
